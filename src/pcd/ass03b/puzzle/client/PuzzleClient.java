package pcd.ass03b.puzzle.client;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import pcd.ass03b.puzzle.server.PuzzleService;

public class PuzzleClient {

	public static void main(final String[] args) {

		try {

			final String host = args.length < 1 ? null : args[0];
			final Registry registry = LocateRegistry.getRegistry(host);
			final PuzzleService puzzleService = (PuzzleService) registry.lookup("puzzleObj");

			final int id = puzzleService.getNextId();
			final SelectionManager selectionManager = new SelectionManager(id);
			final PuzzleBoard puzzle = new PuzzleBoard(puzzleService, selectionManager);
			puzzle.setVisible(true);
			puzzle.addWindowListener(new WindowAdapter() {
				@Override
				public void windowClosing(WindowEvent e) {
					try {
						puzzleService.removeNotifiableClient(id);
					} catch (final RemoteException e1) {
						e1.printStackTrace();
					}
					e.getWindow().dispose();
				}
			});

			final NotifiableClient client = new ClientImpl(id, puzzle);
			UnicastRemoteObject.exportObject(client, 0);

			puzzleService.addNotifiableClient(client);
			
			puzzle.updateTitle(id, puzzleService.getClientColor(id));

		} catch (final Exception e) {
			e.printStackTrace();
		}
	}
}
