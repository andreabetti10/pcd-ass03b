package pcd.ass03b.puzzle.client;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface NotifiableClient extends Remote {

	public void updateBoard() throws RemoteException;

	public int getId() throws RemoteException;

}
