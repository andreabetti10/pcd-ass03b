package pcd.ass03b.puzzle.client;

import java.rmi.RemoteException;

public class ClientImpl implements NotifiableClient {

	private final int id;
	private final PuzzleBoard puzzleBoard;

	public ClientImpl(int id, PuzzleBoard puzzleBoard) {
		this.id = id;
		this.puzzleBoard = puzzleBoard;
	}

	@Override
	public void updateBoard() throws RemoteException {
		puzzleBoard.paintPuzzle();
		puzzleBoard.checkSolution();
	}

	@Override
	public int getId() throws RemoteException {
		return id;
	}

}
