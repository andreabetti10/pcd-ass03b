package pcd.ass03b.puzzle.client;

import java.awt.image.BufferedImage;
import java.awt.image.CropImageFilter;
import java.awt.image.FilteredImageSource;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;

@SuppressWarnings("serial")
public class TileButton extends JButton {

	public TileButton(int rows, int columns, int originalPosition, JFrame frame, BufferedImage image) {
		super(new ImageIcon(frame.createImage(new FilteredImageSource(image.getSource(),
				new CropImageFilter((originalPosition % columns) * image.getWidth(null) / columns,
						(originalPosition / columns) * image.getHeight(null) / rows, image.getWidth(null) / columns,
						image.getHeight(null) / rows)))));
	}
	
}
