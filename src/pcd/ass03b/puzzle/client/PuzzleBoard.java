package pcd.ass03b.puzzle.client;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import pcd.ass03b.puzzle.CustomColor;
import pcd.ass03b.puzzle.Tile;
import pcd.ass03b.puzzle.server.PuzzleService;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;

public class PuzzleBoard extends JFrame {

	private static final long serialVersionUID = 1L;
	private int rows, columns;
	private String imagePath;
	private final JPanel boardPanel = new JPanel();
	private PuzzleService puzzleService;
	private BufferedImage image;
	private SelectionManager selectionManager;

	public PuzzleBoard(PuzzleService puzzleService, SelectionManager selectionManager) {

		try {

			this.puzzleService = puzzleService;
			this.rows = puzzleService.getRows();
			this.columns = puzzleService.getColumns();
			this.imagePath = puzzleService.getImagePath();
			this.selectionManager = selectionManager;

			setTitle("Puzzle");
			setResizable(false);
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

			boardPanel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
			boardPanel.setLayout(new GridLayout(rows, columns, 0, 0));
			getContentPane().add(boardPanel, BorderLayout.CENTER);

			try {
				image = ImageIO.read(new File(imagePath));
			} catch (final IOException ex) {
				image = new BufferedImage(50, 50, ColorSpace.TYPE_RGB);
				SwingUtilities.invokeLater(() -> {
					JOptionPane.showMessageDialog(this, "Could not load image", "Error", JOptionPane.ERROR_MESSAGE);
				});
				return;
			}

			paintPuzzle();

		} catch (final RemoteException e) {
			e.printStackTrace();
		}
	}

	public void paintPuzzle() {

		try {
			boardPanel.removeAll();
			puzzleService.getTiles().forEach(tile -> {
				final TileButton btn = new TileButton(rows, columns, tile.getOriginalPosition(), this, image);
				try {
					btn.setBorder(BorderFactory.createLineBorder(puzzleService.getLastColor(tile.getCurrentPosition())));
				} catch (final RemoteException e) {
					e.printStackTrace();
				}
				boardPanel.add(btn);
				btn.addActionListener(actionListener -> {
					selectionManager.selectTile(tile.getCurrentPosition(), puzzleService);
				});
			});
			pack();
		} catch (final RemoteException e) {
			e.printStackTrace();
		}
	}

	public void checkSolution() {
		try {
			if (puzzleService.getTiles().stream().allMatch(Tile::isInRightPlace)) {
				SwingUtilities.invokeLater(() -> {
					JOptionPane.showMessageDialog(this, "Puzzle Completed!", "", JOptionPane.INFORMATION_MESSAGE);
				});
			}
		} catch (final RemoteException e) {
			e.printStackTrace();
		}
	}
	
	public void updateTitle(int id, CustomColor customColor) {
		setTitle("Puzzle [" + "Player " + id + "] [" + "Color: " + customColor.toString() + "]");
	}
}
