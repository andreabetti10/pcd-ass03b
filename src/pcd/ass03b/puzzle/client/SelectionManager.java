package pcd.ass03b.puzzle.client;

import java.rmi.RemoteException;
import pcd.ass03b.puzzle.server.PuzzleService;

public class SelectionManager {

	private boolean selectionActive = false;
	private int selectedPosition;
	private final int id;

	public SelectionManager(int id) {
		this.id = id;
	}

	public void selectTile(final int position, final PuzzleService puzzleService) {
		
		try {
			if (selectionActive) {
				selectionActive = false;
				puzzleService.swapTiles(selectedPosition, position);
				puzzleService.deselectTile(selectedPosition, id);
			} else {
				selectionActive = true;
				selectedPosition = position;
				puzzleService.selectTile(selectedPosition, id);
			}
			puzzleService.updateClients();
		} catch (final RemoteException e) {
			e.printStackTrace();
		}

	}
}
