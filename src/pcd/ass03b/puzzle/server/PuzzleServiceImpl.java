package pcd.ass03b.puzzle.server;

import java.awt.Color;
import java.rmi.RemoteException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import pcd.ass03b.puzzle.CustomColor;
import pcd.ass03b.puzzle.Tile;
import pcd.ass03b.puzzle.client.NotifiableClient;

public class PuzzleServiceImpl implements PuzzleService {

	private static final SecureRandom RAND = new SecureRandom();

	private final int rows;
	private final int columns;
	private final String imagePath;
	private final List<Tile> tiles = new ArrayList<>();
	private final Map<Integer, NotifiableClient> clients = new HashMap<>();
	// Client colors
	private final Map<Integer, CustomColor> clientColors = new HashMap<>();
	// List of clients for each position
	private final Map<Integer, List<Integer>> selections = new HashMap<>();
	private int lastGenId = 0;

	public PuzzleServiceImpl(final int rows, final int columns, final String imagePath) {
		this.rows = rows;
		this.columns = columns;
		this.imagePath = imagePath;

		final List<Integer> randomPositions = new ArrayList<>();
		IntStream.range(0, rows * columns).forEach(item -> randomPositions.add(item));
		Collections.shuffle(randomPositions);
		IntStream.range(0, rows * columns).forEach(item -> tiles.add(new Tile(item, randomPositions.get(item))));
		Collections.sort(tiles);
		IntStream.range(0, rows * columns).forEach(item -> selections.put(item, new ArrayList<>()));
	}

	@Override
	public void swapTiles(int p1, int p2) throws RemoteException {
		synchronized (tiles) {
			tiles.get(p1).setCurrentPosition(p2);
			tiles.get(p2).setCurrentPosition(p1);
			Collections.sort(tiles);
		}
	}

	@Override
	public List<Tile> getTiles() throws RemoteException {
		return tiles;
	}

	@Override
	public void addNotifiableClient(NotifiableClient client) throws RemoteException {
		clientColors.put(client.getId(), randomColor());
		clients.put(client.getId(), client);
	}
	
	@Override
	public void removeNotifiableClient(int id) throws RemoteException {
		clients.remove(id);
		clientColors.remove(id);
	}

	@Override
	public void updateClients() throws RemoteException {
		clients.values().forEach(client -> {
			try {
				client.updateBoard();
			} catch (final RemoteException e) {
				e.printStackTrace();
			}
		});
	}

	@Override
	public int getRows() throws RemoteException {
		return rows;
	}

	@Override
	public int getColumns() throws RemoteException {
		return columns;
	}

	@Override
	public String getImagePath() throws RemoteException {
		return imagePath;
	}

	@Override
	public void selectTile(int position, int id) throws RemoteException {
		selections.get(position).add(id);
	}

	@Override
	public void deselectTile(int position, int id) throws RemoteException {
		selections.get(position).remove((Integer) id);
	}

	@Override
	public Color getLastColor(int position) throws RemoteException {
		if (!selections.get(position).isEmpty()) {
			final int lastId = selections.get(position).get(selections.get(position).size() - 1);
			if (clientColors.containsKey(lastId)) {
				return clientColors.get(lastId).getColor();
			}
		}
		return Color.GRAY;
	}

	@Override
	public int getNextId() throws RemoteException {
		return lastGenId++;
	}

	@Override
	public CustomColor getClientColor(int id) throws RemoteException {
		return clientColors.get(id);
	}

	private static CustomColor randomColor() {
		final int x = RAND.nextInt(CustomColor.class.getEnumConstants().length);
		return CustomColor.class.getEnumConstants()[x];
	}

}
