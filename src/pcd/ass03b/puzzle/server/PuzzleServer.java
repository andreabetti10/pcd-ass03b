package pcd.ass03b.puzzle.server;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class PuzzleServer {

	private static final int N = 3;
	private static final int M = 5;
	private static final String IMAGE_PATH = "bletchley-park-mansion.jpg";

	public static void main(String args[]) {
		
		try {
			
			final PuzzleService puzzleObj = new PuzzleServiceImpl(N, M, IMAGE_PATH);
			final PuzzleService puzzleObjStub = (PuzzleService) UnicastRemoteObject.exportObject(puzzleObj, 0);

			// Bind the remote object's stub in the registry
			final Registry registry = LocateRegistry.getRegistry();

			registry.rebind("puzzleObj", puzzleObjStub);

			System.out.println("Puzzle object registered.");
			
		} catch (final Exception e) {
			System.err.println("Server exception: " + e.toString());
			e.printStackTrace();
		}
	}
}