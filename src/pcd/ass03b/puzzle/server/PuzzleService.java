package pcd.ass03b.puzzle.server;

import java.awt.Color;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import pcd.ass03b.puzzle.CustomColor;
import pcd.ass03b.puzzle.Tile;
import pcd.ass03b.puzzle.client.NotifiableClient;

public interface PuzzleService extends Remote {

	public void swapTiles(int p1, int p2) throws RemoteException;

	public List<Tile> getTiles() throws RemoteException;

	public void addNotifiableClient(NotifiableClient client) throws RemoteException;
	
	public void removeNotifiableClient(int id) throws RemoteException;

	public void updateClients() throws RemoteException;

	public int getRows() throws RemoteException;

	public int getColumns() throws RemoteException;

	public String getImagePath() throws RemoteException;

	public void selectTile(int position, int id) throws RemoteException;

	public void deselectTile(int position, int id) throws RemoteException;

	public Color getLastColor(int position) throws RemoteException;

	public int getNextId() throws RemoteException;

	public CustomColor getClientColor(int id) throws RemoteException;

}